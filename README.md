## build and run

```
$ cargo build --release
$ ./target/release/next_event_estimation
```

You might have to wait for a while (5min ~ 30min) to have all images rendered.


## key files
* entrance: `src/main.rs`
* next event estimation: `src/ray_tracing/integrators/next_event_estimation.rs`
* direct illumination: `src/ray_tracing/integrators/direct_illumination.rs`
* monte carlo path tracing: `src/ray_tracing/integrators/monte_carlo_path_trace.rs` 