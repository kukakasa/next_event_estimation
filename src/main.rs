use crate::test_cases::*;

mod fundamental;
mod ray_tracing;
mod test_cases;

fn main() {
    for samples in [10, 20, 50, 100, 200, 500] {
        total_tests::test("next_event_estimation", samples);
    }

    for samples in [10, 20, 50] {
        total_tests::test("direct_illumination", samples);
    }

    for samples in [50, 100, 200, 500, 1000, 2000] {
        total_tests::test("monte_carlo", samples);
    }
}
