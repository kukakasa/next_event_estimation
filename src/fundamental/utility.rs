pub use std::f32::consts::PI;

use rand::distributions::Uniform;
use rand::{thread_rng, Rng};
use rand_distr::Distribution;

pub use crate::fundamental::color::Color;
pub use crate::fundamental::point::Point;
pub use crate::fundamental::vector3::Vector3;

pub fn random_u128() -> u128 {
    let mut rng = rand::thread_rng();
    return rng.gen::<u128>();
}

pub fn random_in_range(low: f32, high: f32) -> f32 {
    let mut rng = thread_rng();
    let uniform_distribution = Uniform::new(low, high);
    return uniform_distribution.sample(&mut rng);
}

pub fn random_zero_to_one() -> f32 {
    return random_in_range(0.0, 1.0);
}

impl Vector3 {
    pub fn to_point(&self) -> Point {
        return Point::new(self.x, self.y, self.z);
    }
}
