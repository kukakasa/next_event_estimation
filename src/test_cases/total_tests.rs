use std::sync::Arc;

use crate::cornell_box::cornell_box;
use crate::fundamental::utility::*;
use crate::ray_tracing::cameras::perspective::Perspective;
use crate::ray_tracing::integrators::direct_illumination::DirectIllumination;
use crate::ray_tracing::integrators::monte_carlo_path_trace::MonteCarloPathTrace;
use crate::ray_tracing::integrators::next_event_estimation::NextEventEstimation;
use crate::ray_tracing::renderer::Renderer;

pub fn test(test_type: &str, samples: u32) {
    println!(
        "TESTING: {} for {} sampling (stratified)",
        test_type, samples
    );

    let ppm_name = format!("{}_{}.ppm", test_type, samples);

    let samples_per_dimension = (samples as f32).sqrt() as u32;
    let samples = samples_per_dimension * samples_per_dimension;
    println!("actual samples: {}", samples);

    const WIDTH: usize = 600;
    const HEIGHT: usize = 600;

    let camera_center = Point::new(278.0, 278.0, -800.0);
    let look_at = Point::new(278.0, 278.0, 0.0);
    let direction = look_at - camera_center;

    let camera = Perspective::new(
        camera_center,
        direction,
        Vector3::new(0.0, 1.0, 0.0),
        PI / 4.0,
        PI / 4.0,
    );

    let renderer = match test_type {
        "direct_illumination" => {
            let integrator = DirectIllumination::new(Arc::new(cornell_box()));
            Renderer::new(Arc::new(camera), Arc::new(integrator), samples)
        }
        "monte_carlo" => {
            let integrator = MonteCarloPathTrace::new(Arc::new(cornell_box()));
            Renderer::new(Arc::new(camera), Arc::new(integrator), samples)
        }
        "next_event_estimation" => {
            let integrator = NextEventEstimation::new(Arc::new(cornell_box()));
            Renderer::new(Arc::new(camera), Arc::new(integrator), samples)
        }
        _ => panic!("unknown test type: {}", test_type),
    };

    let image = renderer.render(WIDTH, HEIGHT);
    image.write(&ppm_name);
    println!("image saved to {}", ppm_name);
    println!();
}
