use crate::fundamental::color::Color;
use crate::ray_tracing::intersection::Intersection;
use crate::ray_tracing::material::Material;
use crate::ray_tracing::ray::Ray;

pub struct DiffuseLight {
    emission: Color,
}

impl DiffuseLight {
    pub fn new(_emission: Color) -> DiffuseLight {
        DiffuseLight {
            emission: _emission,
        }
    }
}

impl Material for DiffuseLight {
    fn scatter(&self, _: Ray, _: &Intersection) -> (bool, Ray, Color) {
        return (false, Ray::dummy(), Color::black());
    }

    fn emit(&self, _: &Intersection) -> Color {
        return self.emission;
    }
}
